# Network-Level Validation Rules

In some cases, transactions may be rejected by the network despite the fact that they successfully unlock their inputs, spend valid UTXOs, and do not conflict with other transactions.
That is, a node may receive the transaction and consider it valid, but it may choose not to relay it to its peers or reject it outright.
In particular, custom, or non-standard, transactions are often treated this way by the Bitcoin Cash network at large.
Custom transactions are defined as those which are not considered standard.

## Standard Transactions

Standard transactions are those that:

 - Only have outputs that use [standard locking scripts](../locking-script.md#standard-scripts)
 - Are below the maximum transaction size of 100,000 bytes
 - Have a [recognised version](../transaction-version.md)
 - Have input scripts that only contain push operations
 - Have input scripts with unlocking scripts less or equal to 1650 bytes in length (see scriptSig limit below)
 - Have the total size of [data output](../locking-script.md#data-output) locking scripts no larger than 223 bytes (see [data output size limit](#data-output-size-limit) below)
 - For [multisig](../locking-script.md#multisig) outputs, must have at most 3 parties and at least 1 required party (i.e. 1-of-1 through 3-of-3).
 - Have non-data outputs with amount above the [dust](#dust) threshold
 - Each input of the transaction, must require no more than `((scriptLength + 60) / 43)` [SigChecks](block-level-validation-rules.md#sigchecks)

Be aware, however, that these rules may vary from node-to-node as they are often configurable.
Some nodes may also accept and relay non-standard transactions.
For this reason, among others, it is always wise to send transactions to multiple nodes.

### The input scriptSig limit

The transaction input scriptSig limit must be less or equal to 1650 bytes to be considered standard. The rationale for the number `1650` byte limit was described in the code base as:

> Biggest 'standard' txin is a 15-of-15 P2SH multisig with compressed
> keys. (remember the 520 byte limit on redeemScript size) That works
> out to a (15\*(33+1))+3=513 byte redeemScript, 513+1+15\*(73+1)+3=1627
> bytes of scriptSig, which we round off to 1650 bytes for some minor
> future-proofing. That's also enough to spend a 20-of-20
> CHECKMULTISIG scriptPubKey, though such a scriptPubKey is not
> considered standard)

### Data Output Size Limit

The data output size limit is calculated as follows:

* 1 byte for the `OP_RETURN` [op code](../../script/1script.md#operation-codes-opcodes)
* 2 bytes for a push operation
* Up to 220 bytes of data to be "pushed"

This size limit can be applied to any number of data outputs in the transaction, provided the total size of data output locking scripts does not exceed the limit.
For example, a transaction is allowed to have three locking scripts that are each 74 bytes in length.
However, a transaction with two data outputs each with 112-byte locking scripts would be invalid as `112 * 2 = 224` exceeds the 223-byte limit.

## Dust

In order to limit the propagation of transactions with limited utility, outputs that would be cost-prohibitive to spend are rejected as "dust."
Dust is defined differently for different node implementations but in the simplest cases a threshold of 546 satoshis is used.
Outputs with fewer satoshis than the dust threshold are rejected, along with the transaction they are a part of.
The exception to this is provably unspendable outputs (e.g. data outputs), which always have a dust threshold of zero satoshis.

### Node-Specific Behavior

The nexad client published by Bitcoin Unlimited uses a static threshold of 546 satoshis (except provably non-spendable outputs which are zero).
