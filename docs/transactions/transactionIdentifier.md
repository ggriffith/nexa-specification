# Transaction Identifiers

## Transaction Idem

The transaction idem, or just idem for short, is a serialization of the transaction that does not include input signature scripts.  

Exactly how the input signature scripts unlock a prevout is irrelevant to most applications and does not affect the blockchain's current state (which is captured in the UTXO set).  What matters is whether those scripts successfully unlocked the prevout or not.  This can be inferred by the transaction's presence within a (sufficently deep) block.

The transaction idem is used to form the [outpoints](outpoint.md) (references to prevouts).  This allows child transactions to be signed before parent transactions.  It also prevents many forms of transaction malleability from orphaning the child transaction.  

It should therefore be used in any application that simply cares whether credits or debits have appeared in the blockchain (wallets, for example).

### Transaction Idem Calculation
Serialize the following transaction fields using standard bitcoin serialization algorithms:
* version
* inputs
	* prevout
	* sequence
	* amount
	* NOTE: the satisfier script (scriptSig) is not serialized -- not even as an empty array
* outputs
* locktime


## Transaction Id

The transaction id is a serialization of all the bytes of the transaction.  It should be used when the exact bytes of a transaction must be defined.  In particular, the blockchain must converge to a specific transaction (of many possible malleated variants), to ensure validity of blocks (meeting certain limits), and as a practical defense against accidental chain divergences.

It is currently used within the block's merkle tree of transactions so that a block unambiguously specifies exactly which transaction (of a set of malleated peers) the block is committing into the blockchain.

The id must also be used within network protocols because (for example) it is possible to create both valid and invalid versions of the same malleated transaction.  These transactions have the same idem, but different ids.

### Transaction Id Calculation

1. Create the "satisfiersHash" by double SHA256 hashing the following byte stream:
number of inputs as a little endian 4 byte number
for each input:
  satisfier script (script sig)
  0xFF
2. Calculate the transaction Idem
3. Concatenate the Idem with the satisfiersHash.
4. The transaction Id is the double SHA256 of the result of step 3.
