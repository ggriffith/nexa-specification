
# Outpoints

Outpoints are references to a ledger (UTXO set) entry.

## Construction

An outpoint is formed by the SHA-256 of the transaction idem concatenated with the 4-byte [LE](../misc/endian.md#little-endian) serialized output index.

## Example Code

### C++
```c++
    COutPoint(uint256 txIdemIn, uint32_t outIdx)
    {
        CSHA256Writer sha;
        sha << txIdemIn << outIdx;
        hash = sha.GetHash();
    }
```
