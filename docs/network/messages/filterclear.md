# Request: Filter Clear (“filterclear”)

Remove the installed bloom filter, and therefore disable bloom filtering.

## Message Format
This message has no contents.
