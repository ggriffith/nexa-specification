# Request: Get Addresses (“getaddr”)

Request the information about active peers on the network.  The recipient may reply with peer IP addresses using an [`addr`](addr.md) message.

## Message Format
This message has no contents.
