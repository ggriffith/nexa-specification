# Response: Transaction (“tx”)

Provides the contents of a transaction.

## Message Format

| Field | Length | Format | Description |
|--|--|--|--|
| transaction data | variable | [Transaction](../../transactions/1transaction.md#format) | The full serialized transaction. |
