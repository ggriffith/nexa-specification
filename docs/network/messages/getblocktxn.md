# Request: Get Block Transactions ("getblocktxn")

Requests unknown transactions in a block from a peer.

Upon receiving a [`cmpctblock`](cmpctblock.md) message, a node may determine it is still missing some transactions from the recent block, which it can request using a `getblocktxn` message.
A node receiving a `getblocktxn` message, that has recently sent a `cmpctblock` message for the specified block to this peer, must respond with either a [`blocktxn`](blocktxn.md) message or a [`block`](block.md) message.

## Format

| Field | Length | Format | Description |
|--|--|--|--|
| block hash | 32 bytes | bytes | The hash of the block containing the desired transactions. |
| indexes count | variable | [variable length integer](../../serialisation/variable-length-integer.md) | The number of transactions being requested. |
| indexes | variable | `indexes_count` [variable length integers](../../serialisation/variable-length-integer.md) | The [differentially encoded indexes](cmpctblock.md#differentially-encoded-indexes) of the transactions within the block being requested. |
