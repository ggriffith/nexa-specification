# Project History

The Bitcoin Cash Protocol has evolved since its inception by Satoshi Nakamoto in 2009.
These evolutions have taken form in live network protocol changes, hard forks, and soft forks.
The greater Bitcoin community has attempted to document the protocol many times since the creation of the network.
Other documentation sources include:

- [bitcoin.it](//bitcoin.it)
- [bitcoin.org/en/developer-reference](//bitcoin.org/en/developer-reference)
- [Mastering Bitcoin](//github.com/bitcoinbook/bitcoinbook) <small>(Book by Andreas M. Antonopoulos)</small>
- [BUwiki](//github.com/bitcoin-unlimited/BUwiki)
- and more...

In early 2019, Jonathan Silverblood proposed BUIP121, "Create formal BCH specifications", to Bitcoin Unlimited.
Soon after, the proposal and an initial budget were approved.
Andrew Stone, Lead Developer of Bitcoin Unlimited subsequently began work on documentation and software tooling to create a wiki system backed by git repositories with .md formatted files.  This combination was chosen to promote the re-use of this documentation among the entire community.
Later independently in 2019 at the Bitcoin Cash City conference in Townsville, QLD Australia, some members of the BCH development community, including Jonathan Silverblood, John Nieri, Josh Ellithorpe, and Mark Lundeberg approached Josh Green of Software Verde to lead the effort of a consolidated descriptive specification for the protocol.
After the conference concluded, Software Verde approached Bitcoin Unlimited with an implementation proposal and it was accepted by Andrew Clifford and Andrew Stone.  The proposal included the work of Andrew Stone.

With the funding provided by Bitcoin Unlimited, the Bitcoin Cash Protocol project began as a collaboration within the community, directed by Josh Green in December of 2019.

In June 2022 this documentation was forked and modified to form the Nexa specification by Andrew Stone.
