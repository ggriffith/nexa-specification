Title: Script Machine Registers
Maintainer: Griffith
Status: Draft
Initial Publication Date: 2024-07-13
Latest Revision Date: 2024-07-13
Version: 0.1

# Script Machine Registers
Script machine registers provide random access data into the script machine instead of always using sequential access.

## How would they work
By using two opcodes [OP_LOAD](op-codes/op_load.md) and [OP_STORE](op-codes/op_load.md) we can save up to X variables into script machine registers for easier use later in the script as a more performant alternative to ROLL and PICK. Initially allow for up to 32 (arbitrary value) registers to be used.

Inside an OP_EXEC, the script registers are reset to empty and can be used by the called script.  Upon return, they are reset to their original values.  In other words, the operation behaves as if the OP_EXEC executed inside a completely different script machine.

Script registers are cleared after execution of each script in the traditional and script template execution models.  It is not meaningful in push-only scripts to load script registers.  Attempting to do so will fail the script.

Scripts SHOULD NOT rely on the initial value of a register to be zero.  Scripts SHOULD always load an initial value.

Script machine registers implemented as `std::array<StackItem, 32> arrRegisters; `in the script machine.


## Final Script State
It is not required that all registers be empty for the script to complete successfully. If values remain when the script is finished, they are cleared/ignored.
