# OP codes Syntax

The following syntax is used to describe opcodes and their stack environment.

@*A* *B* *C* **OP_NAME** => *A* *D*

This line describes the execution of the opcode named in bold: **OP_NAME**.  The symbols that appear before the opcode's name represent objects on the stack that are used by this opcode.  These symbols are arranged in the order PUSH operations should be executed to construct the proper stack.  In this example, when OP_NAME is executed, C is the top element on the stack.

An "at" sign (e.g. @) indicates an expectation that the constraint script is NOT likely to be supplying this item, in commonly envisioned use.  Of course, this is not a requirement since the constraint script may supply as many parameters as desired.

The symbols that appear after the arrow (e.g. =>) indicate the result stack.  These symbols are arranged in the order PUSH operations would be executed to construct the same stack produced by executing this opcode.  Therefore, in this example, *D* is at the top of the stack after execution of OP_NAME.

If a symbol appears both before and after the arrow, it is the same symbol.  This is how stack items that are not popped or moved are indicated.
