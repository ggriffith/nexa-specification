# OP_STORE

## Syntax and Stack

*REG_VAL* *REG_NUM* **OP_STORE** =>

- *REG_VAL*: The stack item to be stored in the register
- *REG_NUM*: The register number that the REG_VAL is stored in. Values outside of the range 0-31 will fail the script.  REG_NUM must be a Script Num (not a big number).

## Implementation
```
// get the top two stack items, the top is the register number, the 2nd from the top is to be the register value
int register_number = top stack item
StackItem register_value = 2nd to top stack item

// in the script machine there are only 32 registers (0-31)
// a register number higher than this a failure
if register_number > 31:
    script failure

// store the value in the register
arrRegisters.at(register_num) = register_value
// remove the top two stack items we just used from the stack
pop_the_stack()
pop_the_stack()
```

Script Example:
```
push 10
push 5
OP_STORE # stores the value 10 into register #5
```

## More Information
Read the document on [Script Registers](../script-registers.md) for more information about the uses and reasoning behind OP_LOAD.
