# OP_LOAD

## Syntax and Stack

*REG_NUM* **OP_STORE** => *REG_VAL*

- *REG_NUM*: The register number that the REG_VAL is loaded from. Values outside of the range 0-31 will fail the script.  REG_NUM must be a Script Num (not a big number).
- *REG_VAL*: The stack item from the register. Note: All registers start script execution with a StackElementType::VCH(0) in them.

## Implementation

```
// get the top stack item, this is the register number we want to load the value from
int register_num  = top stack item;
// in the script machine there are only 32 registers (0-31)
// a register number higher than this a failure
if register_num > 31:
    script failure

// load the value in the register
StackItem register_value = arrRegisters.at(register_num);
PopStack(); // pop the register number off the stack
PushStack(value) // push the loaded value on to the stack
```
Script Example:
```
push 5
OP_LOAD # pops the top stack item (5) and replaces it with the contents of that register
```

## More Information
Read the document on [Script Registers](../script-registers.md) for more information about the uses and reasoning behind OP_LOAD.
