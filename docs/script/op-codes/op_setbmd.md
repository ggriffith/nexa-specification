# OP_SETBMD

*Sets the [BigNum](../bignum.md) modulo divisor*  

## Syntax and Stack  

*D* **OP_SETBMD** => <sup>*[?](../opcodesyntax.md)*</sup>  

- *D*: The modulo divisor

#### Binary representation
OP_SETBMD is defined as the single byte 0xeb.

## Operation

This instruction sets the BigNum modulo to the top element on the stack (*D*).  The script fails if *D* is not a BigNum or a little-endian sign-magnitude encoded integer that is greater than 0 and less than or equal to 2^4096 (**R.O1**).  Note that this limit is one larger than the largest possible BigNum, but can be pushed onto the stack as data in little-endian sign-magnitude encoded integer format.

### Implementation Notes
Efficient implementations may want to internally use 0 to denote a BMD of 2^4096.
