# Transaction Ordering

Transactions in a Block are inherently ordered by virtue of their inclusion in the [Merkle Tree](merkle-tree.md).
Furthermore, the [Coinbase Transaction](block.md#coinbase-transaction) is required to be the first Transaction in the Block.
After that, the order of any remaining Transactions in the block use the [Canonical Transaction Ordering](#canonical-transaction-ordering) rule.

## Canonical Transaction Ordering

With canonical transaction ordering (CTOR), the transactions after the coinbase transaction are required to be sorted in lexicographical order by transaction hash. The transaction hash is interpreted as little endian during sort. Blocks that do not follow this constraint will be rejected by the network.

CTOR is also sometimes referred to as LTOR (lexicographical transaction ordering).
